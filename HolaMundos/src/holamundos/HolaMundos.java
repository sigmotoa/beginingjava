/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package holamundos;

/**
 *
 * @author sigmotoa
 */
public class HolaMundos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println("Hola Mundo :)");
        // TODO code application logic here

        //Tipos de datos primitivos. Enteros
        byte edad = 127;
        short year = -32768;
        int id_user = 1001;
        long id_twitter = 9999999999L;  //importante al usar un long, adicionar la L al final

        //Tipos de datos decimales
        float diametro = 50.50F;  //importante adicionar la F para indicar el float
        double precio = 12345.67891;

        //Datos de tipo Texto
        char genero = 'f';

        //Datos Logicos
        boolean isVisible = true;
        boolean funciona = false;

        //Cast  Casteo
        //Es el que se hace usando parentesis para asignar
        byte b = 6;
        short s = b; //Casteo implicito

        b = (byte) s;

        int i = 1;
        double d = 2.5;

        i = (int) d;

        int codigo = 97;
        char codigoAscii = (char) codigo;

        //Arrays
        //Declaraciones de arreglos
        
        int[] arregloInt =new int [2];
        double arregloDouble[];
        
        int [] [] arreglo2D=new int [2][3]; //6 espacios

        int [][][] arreglo3D=new int[3][3][2];//18 espacios
        
        char [][] names={{'M','T','W'},{'M','T','W'}};
        
        
        char [] nombres = new char [4];
        nombres [0]='h';
        nombres [1]='o';
        nombres [2]='l';
        nombres [3]='a';
        
        System.out.println(nombres[0]);
        System.out.println(nombres[1]);
        System.out.println(nombres[2]);
        System.out.println(nombres[3]);
        
        char[][][][] monkey =new char[2][3][2][2];

        monkey[1][0][0][1]='m';
        
        //Operadores Aritmeticos

        int aa, a=1;
        aa=a+a;
        System.out.println("El valor de aa es: "+aa);
        
        double x=2.56;
        int y=9;
        float w=(float)x+y;
        System.out.println(w);
        
        System.out.println(w*2);
        
        double k=4/0.0000002;
        System.out.println(k);
        
        
        System.out.println(7%2);
        
        
        //Operadores de asignación

        /*
        a+=b  --> a=a+b
        a-=b  --> a=a-b
        a*=b  --> a=a*b
        a/=b  --> a=a/b
        a%=b  --> a=a%b
        
        Incremento y decremento
        
        Posfijo
        a--  --> a=a-1 
        a++  --> a=a+1
        
        Prefija
        
        --a Incrementa y guarda
        ++a
        
        
        
        
        
        

        */
        
        int l=3;
        l++;
        ++l;
        
        
        //Imprimiendo usando FOR
        int [] numeros=new int[5];
        
        for (int c = 0; c <=3; c++) 
        {
            numeros[c]=1;
            System.out.println("numeros["+c+"]: "+numeros[c]);
            //System.out.println("C es menor o igual que 5");            
        }
        
        
        //Usando ForEach
        
        //No se puede tener acceso al indice
        for (int m : numeros) {
            System.out.println(m);
            
        }
        
        
        
        
        
        
        byte al=1;
        byte el=1;
        //byte ea=el+al;
        
        int x1=1;
        while (x1<=10) {
            System.out.println(++x);            
            
        }
        
        
        
        
        
        
        
    }

}
