/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amazonviewer.model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author sigmotoa
 */
public class Chapter extends Movie{

    private int id;    
    private int sessionNumber;

    public Chapter(int sessionNumber, String title, String genre, String creator, int duration, short year) {
        super(title, genre, creator, duration, year);
        this.sessionNumber = sessionNumber;
    }

    public int getSessionNumber() {
        return sessionNumber;
    }

    public void setSessionNumber(int sessionNumber) {
        this.sessionNumber = sessionNumber;
    }

    @Override
    public int getId() {
        return this.id; //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String toString() {
        return "\n ::CHAPTER::"+
                "\n Title: " + getTitle()
                + "\n Genre: " + getGenre()
                + "\n Year: " + getYear()
                + "\n Creator: " + getCreator()
                + "\n Duration: " + getDuration();

    }
    @Override
    public Date stratToSee(Date dateI) {

        return dateI;
    }

    @Override
    public void stopToSee(Date dateI, Date dateF) {
        if (dateF.getSeconds() > dateI.getSeconds()) {
            setTimeViewed(dateF.getSeconds() - dateI.getSeconds());

        }
        else
        {
            setTimeViewed(0);
            
        }

    }
    public static ArrayList<Chapter> makeChaptersList()
    {
        ArrayList<Chapter> chapters = new ArrayList();
        
        for (int i = 1; i < 6; i++) {
            chapters.add(new Chapter(1, "BreakingBad", "Action", "Alguien", 45, (short)2010));
            
        }
        
        return chapters;
    }
    
   

   
    
}
