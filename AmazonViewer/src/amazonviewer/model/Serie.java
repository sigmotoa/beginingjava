/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amazonviewer.model;

import java.util.ArrayList;

/**
 *
 * @author sigmotoa
 */
public class Serie extends Film{

    private int id;
    
    private int seassionQuantity;
    private ArrayList<Chapter> chapters;

    public Serie(int seassionQuantity, String title, String genre, String creator, int duration, ArrayList<Chapter> chapters) {
        super(title, genre, creator, duration);
        this.seassionQuantity = seassionQuantity;
        this.chapters=chapters;
        
        
    }

    
    

    public int getId() {
        return id;
    }

   

    public int getSeassionQuantity() {
        return seassionQuantity;
    }

    public void setSeassionQuantity(int seassionQuantity) {
        this.seassionQuantity = seassionQuantity;
    }

    public ArrayList<Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(ArrayList<Chapter> chapters) {
        this.chapters = chapters;
    }
    @Override
    public String toString() {
        return "::SERIE::"+
                "\n Title: " + getTitle()
                + "\n Genre: " + getGenre()
                + "\n Year: " + getYear()
                + "\n Chapters: " + getSeassionQuantity()
                + "\n Duration: " + getDuration();

    }
    
    public static ArrayList<Serie> makeSeriesList()
    {
        ArrayList<Serie> series=new ArrayList();
        for (int i = 0; i < 6; i++) {
            series.add(new Serie(3,"Breaking Bad","Comedia","Alguien",35, Chapter.makeChaptersList()));
            
        }
       return series; 
        
    }
    
    
}
