/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amazonviewer.model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author sigmotoa
 */
public class Movie extends Film implements IViewable {

    private int id;
    private int timeViewed;

    public Movie(String title, String genre, String creator, int duration, short year) {
        super(title, genre, creator, duration);
        setYear(year);
    }

    public void showData() {
        // System.out.println("Titulo: " + this.title);
        //System.out.println("Genero: " + this.genre);
        //System.out.println("Año: " + this.year);

    }

    public int getId() {
        return id;
    }

    public int getTimeViewed() {
        return timeViewed;
    }

    public void setTimeViewed(int timeViewed) {
        this.timeViewed = timeViewed;
    }

    @Override
    public String toString() {
        return "\n ::MOVIE::"
                + "\n Title: " + getTitle()
                + "\n Genre: " + getGenre()
                + "\n Year: " + getYear()
                + "\n Creator: " + getCreator()
                + "\n Duration: " + getDuration();

    }

    @Override
    public Date stratToSee(Date dateI) {

        return dateI;
    }

    @Override
    public void stopToSee(Date dateI, Date dateF) {
        if (dateF.getSeconds() > dateI.getSeconds()) {
            setTimeViewed(dateF.getSeconds() - dateI.getSeconds());

        }
        else
        {
            setTimeViewed(0);
            
        }

    }
    
    public static ArrayList<Movie> makeMoviesList()
    {
        ArrayList<Movie> movies = new ArrayList();
        
        for (int i = 1; i < 6; i++) {
            movies.add(new Movie("Coco "+i, "Animation", "Alguien "+i, 120+(5*i), (short)2017));
            
        }
        
        return movies;
    }
    
    
    
}
