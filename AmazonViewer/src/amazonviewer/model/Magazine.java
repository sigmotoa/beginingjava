/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amazonviewer.model;

import java.util.Date;

/**
 *
 * @author sigmotoa
 */
public class Magazine extends Publication{

    private int id;
    private String title;
    private Date editionDate;
    private String editorial;
    private String[] authors;
    private boolean readed;

    public Magazine(String title, Date editionDate, String editorial) {
        super(title, editionDate, editorial);
    }

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getEditionDate() {
        return editionDate;
    }

    public void setEditionDate(Date editionDate) {
        this.editionDate = editionDate;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String[] getAuthors() {
        return authors;
    }

    public void setAuthors(String[] authors) {
        this.authors = authors;
    }
    @Override
    public String toString() {
        return "::MAGAZINE::"
                + "\n Title: " + getTitle()
                + "\n Edition: " + getEditionDate()
                + "\n Editorial: " + getEditorial()
                ;

    }
    
    

}
