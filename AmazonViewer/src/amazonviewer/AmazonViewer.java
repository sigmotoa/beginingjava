/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amazonviewer;

import amazonviewer.model.Book;
import amazonviewer.model.Chapter;
import amazonviewer.model.Movie;
import amazonviewer.model.Serie;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import makereport.Report;

/**
 *
 * @author sigmotoa
 */
public class AmazonViewer {
static ArrayList<Movie> movies;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        showMenu();

    }

    public static void showMenu() {
        int exit = 1;

        do {
            System.out.println("BIENVENIDOS AMAZON VIEWER");
            System.err.println("");
            System.out.println("Seleciona el número de la opción deseada");
            System.out.println("1. Movies");
            System.out.println("2. Series");
            System.out.println("3. Books");
            System.out.println("4. Magazines");
            System.out.println("5. Report");
            System.out.println("6. Report Today");
            System.out.println("0. Exit");

            
            Scanner read=new Scanner (System.in);
            exit=Integer.valueOf(read.nextLine());  //Parsea el String de entrada a un Integer
            
            
            switch (exit) {
                case 1:
                    showMovies();
//System.out.println("Menu Peliculas");
                    break;
                case 2:
                    showSeries();
//System.out.println("Menu Series");
                    break;
                case 3:
                    showBooks();
                    //System.out.println("Menu Libros");
                    break;
                case 4:
                    showMagazines();
                    //System.out.println("Menu Revistas");
                    break;
                case 0:
                    exit=0;
                    break;
                case 5:
                    makeReport();
                    break;
                case 6:
                    makeReport(new Date());
                    break;
                default:
                    System.out.println("Menu errado");
                    break;

            }

        } while (exit != 1);

    }

    public static void showMovies() {
       int response = 1;
         movies = Movie.makeMoviesList();
        
        do {
            System.out.println("");
            System.out.println("::MOVIES::");
            System.out.println("");
            
            for (int i = 0; i < movies.size(); i++) {
                System.out.println(i+1 + ". "+movies.get(i).getTitle()+ " Visto: "+movies.get(i).isViewed());
                
            }
            System.out.println("0. Regresar al Menu"); System.out.println("");
            
            //Leer la selección del usuario
            Scanner read=new Scanner(System.in);
            response=Integer.valueOf(read.nextLine());
            
            if(response==0)
            {
                showMenu();
            }
            
             Movie movieSelected =movies.get(response-1);
             movieSelected.setViewed(true);
             Date dateI =movieSelected.stratToSee(new Date());
             
             for (int i = 0; i < 50000; i++) {
                 System.out.println(".....");
                
            }
   
             
             //Al terminar de verla
             movieSelected.stopToSee(dateI, new Date());
             
             System.out.println("");
             System.out.println("Viste: "+movieSelected);
             System.out.println("Por: "+movieSelected.getTimeViewed()+" segundos");
            

        } while (response != 0);
    }
static ArrayList<Serie> series;
    public static void showSeries() {
        int response = 1;
        series= Serie.makeSeriesList();

        do {
            System.out.println("");
            System.out.println("::SERIES::");
            System.out.println("");
            
            for (int i = 0; i < series.size(); i++) {
                System.out.println(i+1 + ". "+series.get(i).getTitle()+ " Visto: "+series.get(i).isViewed());
                
            }
            System.out.println("0. Regresar al Menu"); System.out.println("");
            
            //Leer la selección del usuario
            Scanner read=new Scanner(System.in);
            response=Integer.valueOf(read.nextLine());
            
            if(response==0)
            {
                showMenu();
            }
            
            showChapters(series.get(response-1).getChapters());
            Serie serieSelected =series.get(response-1);
             serieSelected.setViewed(true);
             //Date dateI =serieSelected.stratToSee(new Date());
             
             for (int i = 0; i < 50000; i++) {
                 System.out.println(".....");
                
            }
   
             
             //Al terminar de verla
             //movieSelected.stopToSee(dateI, new Date());
             
             System.out.println("");
             System.out.println("Viste: "+serieSelected);
             //System.out.println("Por: "+movieSelected.getTimeViewed()+" segundos");
            

        } while (response != 0);

    }

    public static void showBooks() {
        int response = 1;
        ArrayList<Book> books= Book.makeBooksList();

        do {
            System.out.println("");
            System.out.println("::BOOKS::");
            System.out.println("");
            
            for (int i = 0; i < books.size(); i++) {
                System.out.println(i+1+". "+books.get(i).getTitle()+" Leido: "+books.get(i).isReaded());
                
            }
            System.out.println("0. Regresar al Menu"); System.out.println("");
            
            //Leer la selección del usuario
            Scanner read=new Scanner(System.in);
            response=Integer.valueOf(read.nextLine());
            
            if(response==0)
            {
                showMenu();
            }
             Book bookSelected =books.get(response-1);
             bookSelected.setReaded(true);
             Date dateI =bookSelected.stratToSee(new Date());
             
             for (int i = 0; i < 50000; i++) {
                 System.out.println(".....");
                
            }
   
             
             //Al terminar de verla
             bookSelected.stopToSee(dateI, new Date());
             
             System.out.println("");
             System.out.println("Leiste: "+bookSelected);
             System.out.println("Por: "+bookSelected.getTimeReaded()+" segundos");
            
            

        } while (response != 0);

    }

    public static void showMagazines() {
        int exit = 1;

        do {
            System.out.println("");
            System.out.println("::MAGAZINES::");
            System.out.println("");

        } while (exit != 0);

    }

    public static void showChapters(ArrayList<Chapter> chaptersOfSelectedSerie) {
        int response = 1;
        
        //ArrayList<Chapter> chapters=Chapter.makeChaptersList();
                
        do {
            System.out.println("");
            System.out.println("::CHAPTERS::");
            System.out.println("");
            
            for (int i = 0; i < chaptersOfSelectedSerie.size(); i++) {
                System.out.println(i+1+". "+chaptersOfSelectedSerie.get(i).getTitle()+" Visto: "+chaptersOfSelectedSerie.get(i).isViewed());
                
            }
            System.out.println("0. Regresar al Menu"); System.out.println("");
            
            //Leer la selección del usuario
            Scanner read=new Scanner(System.in);
            response=Integer.valueOf(read.nextLine());
            
            if(response==0)
            {
                showSeries();
            }
            
            Chapter chapterSelected =chaptersOfSelectedSerie.get(response-1);
            chapterSelected.setViewed(true);
            Date dateI=chapterSelected.stratToSee(new Date());
            
            for (int i = 0; i < 50000; i++) {
                 System.out.println(".....");
                
            }
            
             //Al terminar de verla
             chapterSelected.stopToSee(dateI, new Date());
             
             System.out.println("");
             System.out.println("Viste: "+chapterSelected);
             System.out.println("Por: "+chapterSelected.getTimeViewed()+" segundos");

        } while (response != 0);

    }

    public static void makeReport() {
        
        Report report =new Report();
        report.setNameFile("Reporte");
        report.setExtension("txt");
        report.setTitle("::VISTOS::");
        
        String contentReport="";
        
        for (Movie movie : movies) {
            
            if(movie.getIsViewed())
            {
                contentReport+= movie.toString()+"\n";
            }
            
        }
        
        
        report.setContent(contentReport);
        report.makeReport();
        

    }

    public static void makeReport(Date date) {
        
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        String dateString =df.format(date);
                Report report = new Report();
                report.setNameFile("reporte"+dateString);
                report.setExtension("txt");
                report.setTitle("::VISTOS::");
        
        String contentReport="";
        
        for (Movie movie : movies) {
            
            if(movie.getIsViewed())
            {
                contentReport+= movie.toString()+"\n";
            }
            
        }
        
        
        report.setContent(contentReport);
        report.makeReport();

    }

    
}
